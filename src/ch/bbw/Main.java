package ch.bbw;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Aufgabe 1
        Scanner keyboard = new Scanner(System.in);
        String x = keyboard.nextLine();

        System.out.println(x.length());
        if(x.length() % 2 == 0){
            System.out.println("even");
        } else {
            System.out.println("odd");
        }
        System.out.print(x.charAt(0) + "," + " ");
        System.out.println(x.charAt(x.length() -1));

        //Aufgabe 2
        System.out.println("Bitte geben Sie einen String ein: ");
        String s1 = keyboard.nextLine();
        System.out.println("Bitte geben Sie einen zweiten String ein: ");
        String s2 = keyboard.nextLine();

        System.out.println("Der zusammengesetzte String lautet: " + s1 + " " + s2);

        //Aufgabe 3
        String student1 = "Hans";
        String student2 = "Paula";
        System.out.println(student1.compareTo(student2));

        String student3 = "Patrick";
        String student4 = "Anna";
        System.out.println(student3.compareTo(student4));

        String student5 = "Silvan";
        String student6 = "Simon";
        System.out.println(student5.compareTo(student6));

        String student7 = "Hans";
        String student8 = "Hans";
        System.out.println(student7.compareTo(student8));

        String student9 = "Hans";
        String student10 = "hans";
        System.out.println(student9.compareTo(student10));

        //Aufgabe 4
        String wohnung = "wohnung.jpg";
        String email = "hans.muster@bbw.ch";
        String skateboard = "small-skateboard.jpg";

        boolean b1 = wohnung.endsWith(".jpg");
        boolean b2 = email.endsWith(".ch");
        boolean b3 = skateboard.startsWith("small-");

        System.out.println("Wohnung ends with .jpg: " + b1);
        System.out.println("Email ends with .ch: "+ b2);
        System.out.println("Skateboard starts with small-: " + b3);

        String dog1 = "hund";
        String dog2 = "HUND";

        System.out.println("Dog1 has the same letters as Dog2: " + dog1.equalsIgnoreCase(dog2));

        //Aufgabe 5

        System.out.print("Enter your filename (with datatype): ");
        String filename1 = keyboard.nextLine();

        System.out.println("Your filename ends with .jpg: " + filename1.endsWith(".jpg"));

        //Aufgabe 6

        System.out.print("Enter your filename (with datatype ending): ");
        String filename2 = keyboard.nextLine();

        System.out.println("Your filename without datatype ending is: " + filename2.substring(0, filename2.indexOf(".")));

        //Aufgabe 7

        String word = "Wanze";
        String verb = "tanzen";

        String end = "()";
        do {
            System.out.println("Auf der Mauer, auf der Lauer, sitzt 'ne kleine " + word);
            System.out.println("Sieh einmal die " + word + " an, wie die " + word + " " + verb + " kann");
            System.out.println("Auf der Mauer, auf der Lauer, sitzt 'ne kleine " + word);
            word = word.substring(0, word.length() - 1);
            if (verb.endsWith("en")) {
                verb = verb.substring(0, verb.length() - 1);
            }
            verb = verb.substring(0, verb.length() - 1);
            if (word.length() == 0) {
                word = "()";
                verb = "()";
            }
        }
        while(word != end);
        System.out.println("Auf der Mauer, auf der Lauer, sitzt 'ne kleine " + end);
        System.out.println("Sieh einmal die " + end + " an, wie die " + end + " " + end + " kann");
        System.out.println("Auf der Mauer, auf der Lauer, sitzt 'ne kleine " + end);

        keyboard.close();
    }
}
